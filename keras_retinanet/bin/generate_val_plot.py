import matplotlib.pyplot as plt

def generate_plot(path_to_log_file):
    with open("01-02-henkel_retrain_trial2_freezebackbone1.1.log") as file:
        all_lines = file.readlines()

    lines = list(filter(lambda x: "809/809" in x, all_lines))
    total_loss = list(map(lambda x: float(x.split("-")[2].split(":")[1]), lines))
    reg_loss = list(map(lambda x: float(x.split("-")[3].split(":")[1]), lines))
    classifiation_loss = list(map(lambda x: float(x.split("-")[4].split(":")[1]), lines))
    map_lines = list(filter(lambda x: "mAP:" in x, all_lines))
    mAP = list(map(lambda x: float(x.split(":")[1].strip()) * 100, map_lines))

    person_map_lines = list(filter(lambda x: "1847 instances of class person with average precision" in x, all_lines))
    mAP_person = list(map(lambda x: float(x.split(":")[1].strip()) * 100, person_map_lines))

    helmet_map_lines = list(filter(lambda x: "1294 instances of class helmet with average precision" in x, all_lines))
    mAP_helmet = list(map(lambda x: float(x.split(":")[1].strip()) * 100, helmet_map_lines))

    no_helmet_map_lines = list(
        filter(lambda x: "592 instances of class no_helmet with average precision" in x, all_lines))
    mAP_no_helmet = list(map(lambda x: float(x.split(":")[1].strip()) * 100, no_helmet_map_lines))

    jacket_map_lines = list(filter(lambda x: "340 instances of class jacket with average precision" in x, all_lines))
    mAP_jacket = list(map(lambda x: float(x.split(":")[1].strip()) * 100, jacket_map_lines))

    no_jacket_map_lines = list(
        filter(lambda x: "1571 instances of class no_jacket with average precision" in x, all_lines))
    mAP_no_jacket = list(map(lambda x: float(x.split(":")[1].strip()) * 100, no_jacket_map_lines))

    plt.figure(figsize=(12, 12))
    plt.subplot(2, 1, 1), plt.plot(range(0, len(total_loss)), total_loss, c='g', label="Total Loss"), plt.plot(
        range(0, len(reg_loss)), reg_loss, c='r', label="Regression Loss"), plt.plot(range(0, len(classifiation_loss)),
                                                                                     classifiation_loss, c='b',
                                                                                     label="Classification Loss"), plt.xlabel(
        "Epochs"), plt.ylabel("Loss"), plt.title("Losses vs No. Epochs"), plt.legend()
    plt.subplot(2, 1, 2), plt.plot(range(0, len(mAP_person)), mAP_person, label="person"), plt.scatter(
        range(0, len(mAP_person)), mAP_person),
    plt.plot(range(0, len(mAP_helmet)), mAP_helmet, label="helmet"), plt.scatter(range(0, len(mAP_helmet)), mAP_helmet),
    plt.plot(range(0, len(mAP_no_helmet)), mAP_no_helmet, label="no_helmet"), plt.scatter(range(0, len(mAP_no_helmet)),
                                                                                          mAP_no_helmet),
    plt.plot(range(0, len(mAP_jacket)), mAP_jacket, label="jacket"), plt.scatter(range(0, len(mAP_jacket)), mAP_jacket),
    plt.plot(range(0, len(mAP_no_jacket)), mAP_no_jacket, label="no_jacket"), plt.scatter(range(0, len(mAP_no_jacket)),
                                                                                          mAP_no_jacket),
    plt.plot(range(0, len(mAP)), mAP, label="Avg mAP", c='k', linewidth=3), plt.scatter(range(0, len(mAP)), mAP, c='k',
                                                                                        s=20),
    plt.legend()
    #save_name = path_to_log_file.split("/")[-1].split(".")[0] + ".png"
    save_path = path_to_log_file.split(".")[0]+".png"
    plt.savefig(save_path)

if __name__ == "__main__":
    path_to_log_file = "/datadrive/TrainingCodes/keras_retinanet/keras_retinanet/bin/01-02-henkel_retrain_trial2_freezebackbone1.1.log"
    generate_plot(path_to_log_file)

